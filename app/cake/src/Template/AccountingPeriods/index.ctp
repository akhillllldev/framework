<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AccountingPeriod[]|\Cake\Collection\CollectionInterface $accountingPeriods
 */
?>

<div class="accountingPeriods index large-9 medium-8 columns content">
    <h3><?= __('Accounting Periods') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('start_ruler_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('start_month_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('start_year_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('start_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('end_ruler_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('end_month_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('end_year_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('end_date') ?></th>
                <!-- <th scope="col" class="actions"><?= __('Actions') ?></th> -->
            </tr>
        </thead>
        <tbody>
            <?php foreach ($accountingPeriods as $accountingPeriod): ?>
            <tr>
                <td><?= $this->Number->format($accountingPeriod->start_ruler_id) ?></td>
                <td><?= $this->Number->format($accountingPeriod->start_month_id) ?></td>
                <td><?= $this->Number->format($accountingPeriod->start_year_id) ?></td>
                <td><?= h($accountingPeriod->start_date) ?></td>
                <td><?= $accountingPeriod->has('ruler') ? $this->Html->link($accountingPeriod->ruler->ruler, ['controller' => 'Rulers', 'action' => 'view', $accountingPeriod->ruler->id]) : '' ?></td>
                <td><?= $accountingPeriod->has('month') ? $this->Html->link($accountingPeriod->month->month_no, ['controller' => 'Months', 'action' => 'view', $accountingPeriod->month->id]) : '' ?></td>
                <td><?= $this->Number->format($accountingPeriod->end_year_id) ?></td>
                <td><?= h($accountingPeriod->end_date) ?></td>
                <!-- <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $accountingPeriod->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $accountingPeriod->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $accountingPeriod->id], ['confirm' => __('Are you sure you want to delete # {0}?', $accountingPeriod->id)]) ?>
                </td> -->
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
