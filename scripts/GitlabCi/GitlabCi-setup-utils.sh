# This setups the utils for GitlabCi

# 1. Install php7.2
apt-get install -y -qq php7.2

# 2. Install composer
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === 'e0012edf3e80b6978849f5eff0d4b4e4c79ff1609dd1e613307e16318854d24ae64f26d17af3ef0bf7cfb710ca74755a') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php composer-setup.php --install-dir=/usr/local/bin --filename=composer

# Show composer version.
composer --version